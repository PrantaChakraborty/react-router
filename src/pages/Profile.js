import { useEffect } from "react";
import { Route, Switch, useHistory, Link, useRouteMatch } from "react-router-dom";
import ViewProfile from "./ViewProfile";
import EditProfile from "./EditProfile";

const Profile = ({ login }) => {
	const history = useHistory();
	useEffect(() => {
		if (!login) {
			history.push("/"); // redirect to the home page is user is not logged in
		}
	}, []);

	const {path, url} = useRouteMatch()
	return (
		<div>
			<h3>profile page</h3>
			<ul>
				<li>
					<Link to={`${url}/viewprofile`}>ViewProfile</Link>
				</li>

				<li>
					<Link to={`${url}/editprofile`}>Edit Profile</Link>
				</li>
			</ul>
			<Switch>
				<Route path= {`${path}/viewprofile`}component={ViewProfile} />
				<Route path={`${path}/editprofile`} component={EditProfile}/>
			</Switch>
		</div>
	);
};

export default Profile;
