import React from "react";
import { useLocation, useParams } from "react-router-dom";

const Post = ({match}) => {
    //  for showing page by id or some text wise use useParams hooks
	const { id } = useParams();
    const query = new URLSearchParams(useLocation().search)
	return (
		<div>
			<h3>Id is: {id}</h3>
            <h3>{query.get('first')}</h3>
		</div>
	);
};

export default Post;
