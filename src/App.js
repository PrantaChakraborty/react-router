import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useState } from "react";
import "./App.css";
import Header from "./components/Header";
import Home from "./pages/Home";
import About from "./pages/About";
import Profile from "./pages/Profile";
import NotFound from "./pages/NotFound";
import Post from "./pages/Post";

function App() {
	const [login, setlogin] = useState(false);
	return (
		<div>
			{/* <BrowserRouter basename='anythin'>
    the base name is used for like anything/home or anything/profle
     <BrowserRouter forceRefresh> is used for force reload the window
    */}
			<BrowserRouter>
				<div className="App">
					<Header />
					<button onClick={() => setlogin(!login)}>
						{login ? "Logout" : "Login"}
					</button>
					<Switch>
						<Route path="/" component={Home} exact></Route>
						<Route path="/about" component={About}></Route>
						{/* if user is login then he can be able to visit profile page otherwise he will be redirect to the home page */}
						{/* <Route path="/profile">
							{login ? <Profile /> : <Redirect to="/" />}
						</Route> */}
            {/* another way to do that passing the props in the profile page */}
            <Route path='/profile'> <Profile login={login} /></Route>
						<Route path="/post/:id" component={Post}></Route>
						<Route component={NotFound}></Route>
					</Switch>
				</div>
			</BrowserRouter>
		</div>
	);
}

export default App;
